<?php
session_start();
require_once "pdo.php";
//require_once "utility.php";

header('Content-Type: application/json; charset=utf-8');

if (! isset($_SESSION['user_id'])) {
    die('Access Denied');
}

$term = $_GET['term'];
$stmt = $pdo->prepare('SELECT school FROM Institution WHERE school LIKE :prefix');
//$stmt->execute(array( ':prefix' => $_REQUEST['term']."%"));
$stmt->execute(array( ':prefix' => $term."%"));
$retval = array();
while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
  $retval[] = $row['school'];
//  error_log($row['school']);
}

echo(json_encode($retval, JSON_PRETTY_PRINT));

?>
