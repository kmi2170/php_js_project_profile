<?php

function validatePosition()
{
    for ($i = 1; $i <= 9; $i++) {
        if (! isset($_POST['year'.$i])) {
            continue;
        }
        if (! isset($_POST['desc'.$i])) {
            continue;
        }

        if (empty($_POST['year'.$i]) ||  empty($_POST['desc'.$i])) {
            return "All fields are quired";
        }

        if (! is_numeric($_POST['year'.$i])) {
            return "year".$i." must be numeric";
        }
    }
    return true;
}

function validateEducation()
{
    for ($i = 1; $i <= 9; $i++) {
        if (! isset($_POST['edu_year'.$i])) {
            continue;
        }
        if (! isset($_POST['school'.$i])) {
            continue;
        }

        if (empty($_POST['edu_year'.$i]) ||  empty($_POST['school'.$i])) {
            return "All fields are quired";
        }

        if (! is_numeric($_POST['edu_year'.$i])) {
            return "edu_year".$i." must be numeric";
        }
    }
    return true;
}

function validateUrl($url)
{
//    if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
    $http_status = url_exists($_POST['url']);
    if ($http_status == 403) {
        return "URL doesn't exits";
    }
    return true;
}


function url_exists($url)
{
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch);

    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return $http_status;
}


function flushMessages()
{
    if (isset($_SESSION['success'])) {
        echo('<p style="color: green">'.$_SESSION['success']."</p>\n");
        unset($_SESSION['success']);
    }
    if (isset($_SESSION['error'])) {
        echo('<p style="color: red">'.$_SESSION['error']."</p>\n");
        unset($_SESSION['error']);
    }
}
