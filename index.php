<?php
session_start();
require_once "pdo.php";
require_once "utility.php";

echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            table {
                border-collapse: collapse;
                border: 3px solid black;
                width: 60%;
                text-align: center;
            }
            th {
                background-color: lightgrey;
            }
            th, td  {
                border: 1px solid black;
            }
            #action {
                color: grey;
            }
            .result {
                color: red;
            }
            #search {
                background-color: lightpink;
            }
        </style>
    </head>
    <body>
        <h1>Resume Registry</h1>

        <?php flushMessages(); ?>

        <?php if (isset($_SESSION['user_id'])) { ?>
        <h3> Your user name: <?= htmlentities($_SESSION['name']) ?> </h3>
        <p>
        <a href="add.php">Add New Resume</a> /
        <a href="logout.php">Log Out</a>
        </p>
        <?php } else { ?>
        <p>
        <a href="login.php">Log In</a>
        </p>
        <?php } ?>

        <?php
        $stmt = $pdo->query("SELECT COUNT(*) FROM Profile");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (! $row === false) {
            $count = $row['COUNT(*)'];
        }
        $inc = 5;
        $pages = intdiv($count, $inc);
        if ($count%$inc !==0) {
            $pages +=1;
        }
//        echo "pages", $pages;
        $firstPage = 1;
        $lastPage = $pages;
        if (! isset($_SESSION['currentPage'])) {
            $_SESSION['currentPage'] = 1;
        }

        if (isset($_POST['backward']) && ! empty($_POST['backward'])) {
            if ($_SESSION['currentPage'] > 1) {
                $_SESSION['currentPage'] = $_SESSION['currentPage'] - 1;
            }
            header("Location: index.php");
            return;
        }
        if (isset($_POST['forward']) && ! empty($_POST['forward'])) {
            if ($_SESSION['currentPage'] < $lastPage) {
                $_SESSION['currentPage'] = $_SESSION['currentPage'] + 1;
            }
            header("Location: index.php");
            return;
        }
        ?>

        <form method="post">
        <?php if ($_SESSION['currentPage'] !== $firstPage) { ?>
            <input type="submit" name="backward" value="<">
        <?php } ?>
            Page <?= $_SESSION['currentPage'] ?> of <?= $lastPage ?>
        <?php if ($_SESSION['currentPage'] !== $lastPage) { ?>
            <input type="submit" name="forward" value=">">
        <?php } ?>
        </form>

        <?php
            $sql = "SELECT profile_id, user_id, first_name, last_name, email FROM Profile";

            $inc = 5;
            $from = ($_SESSION['currentPage'] -1)*$inc;
            $sql = $sql." LIMIT ".$from.", ".$inc;
            echo("<p><?= $sql ?></p>");
        ?>

        <table>
            <tr>
                <th>Id</th><th>First Name</th>
                <th>Last Name</th><th>Email</th>
                <th>Action</th>
            </tr>

        <?php
            $stmt = $pdo->query($sql);
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<tr><td>";
                echo(htmlentities($row['profile_id']));
//                $_SESSION['profile_id'] = $row['profile_id'];
                echo "</td><td>";
                echo(htmlentities($row['first_name']));
                echo "</td><td>";
                echo(htmlentities($row['last_name']));
                echo "</td><td>";
                echo(htmlentities($row['email']));

                echo "</td><td>";
                echo('<a href="view.php?profile_id='.$row['profile_id'].'">View</a>'.' / ');
                if (isset($_SESSION['user_id']) && $row['user_id'] == $_SESSION['user_id']) {
                    //echo('<a href="edit.php?id='.$row['profile_id'].'">Edit</a>'.' / ');
                    echo('<a href="edit.php?profile_id='.$row['profile_id'].'">Edit</a>'.' / ');
                    echo('<a href="delete.php?profile_id='.$row['profile_id'].'">Delete</a>');
                } else {
                    echo ('<span id="action">Edit / Delete</span>');
                }

                echo "</td></tr>\n";

            }


        ?>
        </table>

        <p>
        <form method="post">
            Search Keyword: <input type="text" name="keyword">
            Choose field:
            <select name="field">
                <option value="first_name">First Name</option>
                <option value="last_name">Last Name</option>
                <option value="email">email</option>
                <option value="headline">Headline</option>
                <option value="summary">Summary</option>
                <option value="school">Education</option>
            </select>
            <br>
            Exact Search:
            <select name="exact">
                <option value="off">OFF</option>
                <option value="on">ON</option>
            </select>
            <br>
            <input type="submit" value="Search">
        </form>
        </p>

        <?php
        if (isset($_POST['keyword']) && isset($_POST['field'])) {
            if (! empty($_POST['keyword']) && ! empty($_POST['field'])) {
                $_SESSION['field'] =$_POST['field'];
                $_SESSION['keyword'] =$_POST['keyword'];
                $_SESSION['exact'] =$_POST['exact'];
                header("Location: index.php");
                return;
            }
        }

        if (isset($_SESSION['keyword']) && isset($_SESSION['field'])) {
            if (! empty($_SESSION['keyword']) && ! empty($_SESSION['field'])) {
                $fl = $_SESSION['field'];
                $kw = $_SESSION['keyword'];

                if ($_SESSION['exact'] === 'off') {
                    $kw = "%".$kw."%";
                }
                unset($_SESSION['exact']);

                if ($fl === 'school') {
                    $sql = "SELECT * FROM Education JOIN Institution WHERE Education.institution_id=Institution.institution_id AND school LIKE :kw";
                    //$sql = "SELECT * FROM Education JOIN Institution WHERE Education.institution_id=Institution.institution_id AND LOWER(school) LIKE LOWER(:kw)";
                } else {
                    $sql = "SELECT * FROM Profile WHERE ".$fl." LIKE :kw";
                }
                $stmt = $pdo->prepare($sql);
                $stmt->execute(array(':kw' => $kw));
//                echo("<p>".$sql."</p>");
        //        $stmt->execute(array(':fd' => $fl, ':kw' => $kw));
        //        var_dump($stmt->fetch(PDO::FETCH_ASSOC));
        ?>
            <table id="search">
        <?php
                $flag = 1;
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if ($flag === 1) {
                        echo "Found";
        ?>
                    <tr>
                        <th>Id</th><th><?= $fl ?></th>
                    </tr>
        <?php
                    }
                    $flag = 0;
                    echo "<tr><td>";
                    echo(htmlentities($row['profile_id']));
                    echo "</td><td>";
                    $kw = $_SESSION['keyword'];
                    if ($kw === 'name') $kw = 'education';
                    $result = str_replace($kw, "<span class='result'>".$kw."</span>", htmlentities($row[$fl]));
                    //echo(htmlentities($result));
                    echo($result);
                    echo "</td></tr>\n";
                }
                if ($flag === 1) {
                    echo "No match";
                }
        ?>
            </table>
        <?php
            }
        }
        unset($_SESSION['keyword']);
        unset($_SESSION['field']);
        ?>

    </body>
</html>
