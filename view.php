<?php
require_once "pdo.php";

session_start();
echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

if (! isset($_GET['profile_id'])) {
//if (! isset($_REQEST['profile_id'])) {
    $_SESSION['error'] = "Missing Profile Id";
    header("Location: index.php");
    return;
}

$sql = "SELECT * FROM Profile WHERE profile_id=:id";
echo("<p>".$sql."</p>");
$stmt = $pdo->prepare($sql);
$stmt->execute(array(':id' => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ($row === false) {
    $_SESSION['error'] = "Bad Profile Id";
    header("Location: index.php");
    return;
}

$stmt = $pdo->prepare('SELECT * FROM Position WHERE profile_id =:id ORDER BY rank');
$stmt->execute(array(':id' => $_GET['profile_id']));

$stmt_edu = $pdo->prepare('SELECT * FROM Education JOIN Institution WHERE profile_id =:id AND Education.institution_id=Institution.institution_id ORDER BY rank');
$stmt_edu->execute(array(':id' => $_GET['profile_id']));

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            #msg{
                color: red;
            }
            th, td  {
                border: 1px solid black;
                border-color: grey;
            }
        </style>
    </head>
    <body>
        <h1>Resume</h1>

        <?php
          $fname = htmlentities($row['first_name']);
          $lname = htmlentities($row['last_name']);
          $email = htmlentities($row['email']);
          $head = htmlentities($row['headline']);
          $sum = htmlentities($row['summary']);
          $url = htmlentities($row['url']);
        ?>

        <table>
            <tr>
                <td><b>First Name:</b></td>
                <td><?= $fname ?></td>
            </tr>
            <tr>
                <td><b>Last Name:</b></td>
                <td><?= $lname ?></td>
            </tr>
            <tr>
                <td><b>Email:</b></td>
                <td><?= $email ?></td>
            </tr>
            <tr>
                <td><b>Headline:</b></td>
                <td><?= $head ?></td>
            </tr>
            <tr>
                <td><b>Summary:</b></td>
                <td><?= $sum ?></td>
            </tr>
            <tr>
                <td><b>URL(optional):</b></td>
                <td><?= $url ?></td>
            </tr>
        </table>

        <p>Education:</p>
        <ul>
        <?php
        while ($row = $stmt_edu->fetch(PDO::FETCH_ASSOC)) {
            $year = htmlentities($row['year']);
            $school = htmlentities($row['school']);
        ?> <li>
            <?php echo($year.' / '.$school); ?>
            </li>
        <?php } ?>
        </ul>

        <p>Positions:</p>
        <ul>
        <?php
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $year = htmlentities($row['year']);
            $description = htmlentities($row['description']);
        ?> <li>
            <?php echo($year.' / '.$description); ?>
            </li>
        <?php } ?>
        </ul>

        <p><a href="index.php">Back to Index</a></p>

    </body>
</html>
