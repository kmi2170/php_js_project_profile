<?php
session_start();
require_once "pdo.php";
require_once "utility.php";

echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

if (!isset($_SESSION['user_id'])) {
    die('Access Denied');
}

if (isset($_POST['clear'])) {
    header("Location: add.php");
    return;
}

if (isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email']) && isset($_POST['headline']) && isset($_POST['summary'])) {
    if (! empty($_POST['first_name']) && ! empty($_POST['last_name']) && ! empty($_POST['email']) && ! empty($_POST['headline']) && ! empty($_POST['summary'])) {

        if (! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error'] = "Not a valid email address";
            header("Location: login.php");
            return;
        }

        if (! empty($_POST['url'])) {
//    if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
            $msg = validateUrl($_POST['url']);
            if (is_string($msg)) {
                $_SESSION['error'] = $msg;
                header("Location: add.php");
                return;
            }
            $value_url = $_POST['url'];
        } else {
            $value_url = NULL;
        }

        $stmt = $pdo->prepare('INSERT INTO Profile
            (user_id, first_name, last_name, email, headline, summary, url)
            VALUES ( :uid, :fn, :ln, :em, :he, :su, :ul)');
        $stmt->execute(
            array(
            ':uid' => $_SESSION['user_id'],
            ':fn' => $_POST['first_name'],
            ':ln' => $_POST['last_name'],
            ':em' => $_POST['email'],
            ':he' => $_POST['headline'],
            ':su' => $_POST['summary'],
            ':ul' => $value_url)
        );

        $msg = validatePosition();
        if (is_string($msg)) {
            $_SESSION['error'] = $msg;
            header("Location: add.php");
        }

        $msg = validateEducation();
        if (is_string($msg)) {
            $_SESSION['error'] = $msg;
            header("Location: add.php");
        }

        $profile_id = $pdo -> lastInsertId();

 //       $rank = 1;
        for ($i = 1; $i <= 9; $i++) {
            if (! isset($_POST['year'.$i])) continue;
            if (! isset($_POST['desc'.$i])) continue;

            $rank = $i;
            $year = $_POST['year'.$i];
            $desc = $_POST['desc'.$i];

//            echo $rank."/".$year."/".$desc."\n";
            $stmt = $pdo->prepare('INSERT INTO Position (profile_id, rank, year, description) VALUES ( :pid, :rank, :year, :desc)');

            $stmt->execute(array(
              ':pid' => $profile_id,
              ':rank' => $rank,
              ':year' => $year,
              ':desc' => $desc)
            );
//            $rank++;
        }

        for ($i = 1; $i <= 9; $i++) {
            if (! isset($_POST['edu_year'.$i])) continue;
            if (! isset($_POST['school'.$i])) continue;

            $rank = $i;
            $year = $_POST['edu_year'.$i];
            $school = $_POST['school'.$i];

            $stmt = $pdo->prepare('SELECT * FROM Institution WHERE school = :school');
            $stmt->execute(array( ':school' => $school ));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row === false) {
                $stmt = $pdo->prepare('INSERT INTO Institution (school) VALUES (:school)');
                $stmt->execute(array( ':school' => $school ));
                $institution_id = $pdo -> lastInsertId();
                echo "false\n";
            } else {
                $institution_id = $row['institution_id'];
                var_dump($row);
                echo "true\n";
            }
            echo "institution_id ", $institution_id,"\n";

            $stmt = $pdo->prepare('INSERT INTO Education (profile_id, institution_id, rank, year) VALUES ( :pid, :iid, :rank, :year)');

            $stmt->execute(array(
              ':pid' => $profile_id,
              ':iid' => $institution_id,
              ':rank' => $rank,
              ':year' => $year)
            );
        }


        $_SESSION['success'] = "Record Inserted";
        header("Location: index.php");
        return;
    } else {
        $_SESSION['error'] = "All fields are required";
        header("Location: add.php");
        return;
    }
}

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
        </style>
        <script type="text/javascript" src="../../../jquery.min.js"></script>
        <script type="text/javascript" src="../../../jquery-ui.min.js"></script>
    </head>
    <body>
        <h1>Add New Resume</h1>
        <h3> Your User Name: <?= htmlentities($_SESSION['name']) ?></h3>

        <?php flushMessages(); ?>

        <form method="post">
            <p>First Name: <input type="text" name="first_name"></p>
            <p>Last Name: <input type="text" name="last_name"></p>
            <p>Email: <input type="text" name="email"></p>
            <p>Headline: <input type="text" name="headline"></p>
            <textarea name="summary" rows="5" cols="80">Enter summary here...</textarea>
            <p>Url for Image (optional): <input type="url" name="url"></p>
            <p>Education: <input type="button" id="addEducation" value="+">
            <div id="education_fields"></div></p>
            <p> Position: <input type="button" id="addPosition" value="+">
            <div id="position_fields"></div><p>
            <input type="submit" value="Add">
            <input type="submit" name="clear" value="Clear">
            <a href="index.php">Cancel</a>
        </form>

        <script type="text/javascript">
            $(document).ready(function() {
                window && console.log('Document ready called');
                countPos = 0;
                $('#addPosition').click(function(event) {
                    event.preventDefault();
                    if (countPos >= 9) {
                        alert('Maximum of nine positon entries exceeded');
                        return;
                    }
                    countPos++;
                    window && console.log('Adding Position '+countPos);

                    var source = $('#pos_template').html();
                    $('#position_fields').append(source.replace(/@COUNT@/g, countPos))
                });

                countEdu = 0;
                $('#addEducation').click(function(event) {
                    event.preventDefault();
                    if (countEdu >= 9) {
                        alert('Maximum of nine education entries exceeded');
                        return;
                    }
                    countEdu++;
                    window && console.log('Adding Education '+countEdu);

                    var source = $('#edu_template').html();
                    $('#education_fields').append(source.replace(/@COUNT@/g, countEdu))

                    $('.school').autocomplete({ source: "school.php" });
                });

            });
        </script>

        <script id="edu_template" type="text">
            <div id="edu@COUNT@">
                <p>
                Year: <input type="text" name="edu_year@COUNT@" value="">
                <input type="button" value="-" onclick="$('#edu@COUNT@').remove(); return false;">
                </p>
                School: <input type="text" name="school@COUNT@" class="school" value="">
            </div>

        </script>
        <script id="pos_template" type="text">
                <div id="position@COUNT@">
                <p>
                Year: <input type="text" name="year@COUNT@" value="">
                <input type="button" value="-" onclick="$('#position@COUNT@').remove(); return false;"></p>
                <textarea name="desc@COUNT@" rows="5" cols="80"></textarea>
        </script>

    </body>
</html>
