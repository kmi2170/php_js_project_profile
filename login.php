<?php
require_once "pdo.php";
session_start();

echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);
echo "<br>";

if (isset($_POST['cancel'])) {
    header("Location: index.php");
    return;
}

//var_dump( (! empty($_POST['name']) && ! empty($_POST['pwd'])) );
//var_dump( (isset($_POST['name']) && isset($_POST['pwd'])) );

if (isset($_POST['name']) && isset($_POST['pwd'])) {
    unset($_SESSION['name']);
    unset($_SESSION['pwd']);

    if (! empty($_POST['name']) && ! empty($_POST['pwd'])) {
//    if (strlen($_SESSION['name']) > 0 && strlen($_SESSION['pwd'] > 0)) {
        if (! filter_var($_POST['name'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error'] = "Not a valid email address";
            header("Location: login.php");
            return;
        } else {
//            $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';
            // password: php123
            $salt = 'XyZzy12*_';
            $md5 = hash('md5', $salt.$_POST['pwd']);
            $stmt = $pdo->prepare('SELECT user_id, name FROM users2
                WHERE email = :em AND password = :pw');
            $stmt->execute(array( ':em' => $_POST['name'], ':pw' => $md5));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row !== false) {
                $_SESSION['name'] = $row['name'];
                $_SESSION['user_id'] = $row['user_id'];
                header("Location: index.php");
                return;
            } else {
                //         error_log("Login fail ".$_POST['name']);
                $_SESSION['error'] = "User name and password don't match";
                header("Location: login.php");
                return;
            }
        }
    } else {
        $_SESSION['error'] = "User name and password are required";
        header("Location: login.php");
        return;
    }
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Log in</title>
    <style media="screen">
      body {
        font-family: Arial;
      }
      #msg {
        color: red;
      }
    </style>
  </head>
  <body>

    <h1>Please Log In</h1>

    <?php
      if (isset($_SESSION['error'])) {
          echo("<p id='msg'>".$_SESSION['error']."</p>");
          unset($_SESSION['error']);
      }
    ?>

    <form method="post">
        <p>Email: <input type="text" name="name" id="name"></p>
        <p>Password: <input type="password" name="pwd" id="pwd"></p>
        <input type="submit" value="Log in" onclick="return doValidate();">
        <input type="submit" value="Cancel" name="cancel">
    </form>

    <script type="text/javascript">
        function doValidate() {
            try {
                nm = document.getElementById('name').value;
                pw = document.getElementById('pwd').value;
                console.log("validating nm, pw: "+nm+" / "+pw);
                if( nm == null || nm == "" || pw == null || pw == "" ) {
                    alert('Both fields are required');
                    return false;
                } else {
                    if ( nm.indexOf("@") == -1 ) {
                        alert('Email address must contain "@"');
                        return false;
                    } if ( nm.indexOf(".") == -1 ) {
                        alert('Email address must contain "."');
                        return false;
                    } else {
                        return true;
                    }
                }
            } catch(err) {
                return true;
            }
            return true;
        }
    </script>

  </body>
</html>
