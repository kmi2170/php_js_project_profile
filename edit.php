<?php
require_once "pdo.php";
require_once "utility.php";

session_start();
echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

if (!isset($_SESSION['user_id'])) {
    die('Access Denied');
}

if (! isset($_GET['profile_id'])) {
    $_SESSION['error'] = "Missing Profile Id";
    header("Location: index.php");
    return;
}
/*
if (isset($_POST['reset'])) {
    header("Location: edit.php");
    return;
}
*/

$sql = "SELECT * FROM Profile WHERE profile_id=:id";
echo("<p>".$sql."</p>");
$stmt = $pdo->prepare($sql);
$stmt->execute(array(':id' => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ($row === false) {
    $_SESSION['error'] = "Bad Profile Id";
    header("Location: index.php");
    return;
}

if (isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email']) && isset($_POST['headline']) && isset($_POST['summary'])) {
    if (! empty($_POST['first_name']) && ! empty($_POST['last_name']) && ! empty($_POST['email']) && ! empty($_POST['headline']) && ! empty($_POST['summary'])) {

        if (! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error'] = "Not a valid email address";
            header("Location: edit.php?profile_id=".$_GET['profile_id']);
            return;
        }

        if (! empty($_POST['url'])) {
//    if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
            $msg = validateUrl($_POST['url']);
            if (is_string($msg)) {
                $_SESSION['error'] = $msg;
                header("Location: edit.php?profile_id=".$_GET['profile_id']);
                return;
            }
            $value_url = $_POST['url'];
        } else {
            $value_url = NULL;
        }


        $stmt = $pdo->prepare('UPDATE Profile SET first_name=:fn, last_name=:ln, email=:em, headline=:he, summary=:su, url=:ul WHERE profile_id=:id');
        $stmt->execute(
            array(
            ':id' => $_GET['profile_id'],
            ':fn' => $_POST['first_name'],
            ':ln' => $_POST['last_name'],
            ':em' => $_POST['email'],
            ':he' => $_POST['headline'],
            ':su' => $_POST['summary'],
            ':ul' => $value_url)
        );

        $msg = validatePosition();
        if (is_string($msg)) {
            $_SESSION['error'] = $msg;
//            $_SESSION['error'] = "All fields are required";
            header("Location: edit.php?profile_id=".$_GET['profile_id']);
            return;
        }

        $msg = validateEducation();
        if (is_string($msg)) {
            $_SESSION['error'] = $msg;
//            $_SESSION['error'] = "All fields are required";
            header("Location: edit.php?profile_id=".$_GET['profile_id']);
            return;
        }

 //       $rank = 1;
        $numPos = $_SESSION['numPos'];
        $pos_ids = $_SESSION['pos_ids'];
        $count = 0;
        for ($i = 1; $i <= 9; $i++) {
            if (! isset($_POST['year'.$i])) continue;
            if (! isset($_POST['desc'.$i])) continue;

            $rank = $i;
            $year = $_POST['year'.$i];
            $desc = $_POST['desc'.$i];

//            echo $rank."/".$year."/".$desc."\n";
//            echo $numPos, $pos_ids, "\n";
//            exit;
            if ($i <= $numPos) {
                $stmt = $pdo->prepare('UPDATE Position SET rank=:rank, year=:year, description=:desc WHERE position_id=:posid');
                $stmt->execute(array(
                  ':posid' => $pos_ids[$count],
                  ':rank' => $rank,
                  ':year' => $year,
                  ':desc' => $desc)
                );
                $count++;
            } else {
                $stmt = $pdo->prepare('INSERT INTO Position (profile_id, rank, year, description) VALUES ( :pid, :rank, :year, :desc)');
                $stmt->execute(array(
                  ':pid' => $_GET['profile_id'],
                  ':rank' => $rank,
                  ':year' => $year,
                  ':desc' => $desc)
                );
            }
        }
        while ($count < $numPos) {
            $stmt = $pdo->prepare('DELETE FROM Position WHERE position_id=:posid');
            $stmt->execute(array(':posid' => $pos_ids[$count]));
            $count++;
        }

        $stmt = $pdo->prepare('DELETE FROM Education WHERE profile_id=:pid');
        $stmt->execute(array(':pid' => $_GET['profile_id']));
        for ($i = 1; $i <= 9; $i++) {
            if (! isset($_POST['edu_year'.$i])) continue;
            if (! isset($_POST['school'.$i])) continue;

            $rank = $i;
            $year = $_POST['edu_year'.$i];
            $school = $_POST['school'.$i];

            $stmt = $pdo->prepare('SELECT * FROM Institution WHERE school = :school');
            $stmt->execute(array( ':school' => $school ));
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row === false) {
                $stmt = $pdo->prepare('INSERT INTO Institution (school) VALUES (:school)');
                $stmt->execute(array( ':school' => $school ));
                $institution_id = $pdo -> lastInsertId();
                echo "false\n";
            } else {
                $institution_id = $row['institution_id'];
                var_dump($row);
                echo "true\n";
            }
//            echo "institution_id ", $institution_id,"\n";

            $stmt = $pdo->prepare('INSERT INTO Education (profile_id, institution_id, rank, year) VALUES ( :pid, :iid, :rank, :year)');

            $stmt->execute(array(
              ':pid' => $_GET['profile_id'],
              ':iid' => $institution_id,
              ':rank' => $rank,
              ':year' => $year)
            );
        }

        $_SESSION['success'] = "Record Saved";
        header("Location: index.php");
        return;
    } else {
        $_SESSION['error'] = "All fields are required";
        header("Location: edit.php?profile_id=".$_GET['profile_id']);
        return;
    }
}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            #msg{
                color: red;
            }
        </style>
        <script type="text/javascript" src="../../../jquery.min.js"></script>
        <script type="text/javascript" src="../../../jquery-ui.min.js"></script>
        </script>
    </head>
    <body>
        <h1>Edit Resume</h1>
        <h3> Your User Name: <?= htmlentities($_SESSION['name']) ?></h3>

        <?php
          flushMessages();

          $fname = htmlentities($row['first_name']);
          $lname = htmlentities($row['last_name']);
          $email = htmlentities($row['email']);
          $head = htmlentities($row['headline']);
          $sum = htmlentities($row['summary']);
          $url = htmlentities($row['url']);

        ?>

        <form method="post" id="usrform">
            <p>First Name: <input type="text" name="first_name" value=<?= $fname ?>></p>
            <p>Last Name: <input type="text" name="last_name" value="<?= $lname ?>"></p>
            <p>Email: <input type="text" name="email" value="<?= $email ?>"></p>
            <p>Headline: <input type="text" name="headline" value="<?= $head ?>"></p>
            <textarea name="summary" rows="5" cols="80" form="usrform"><?= $sum ?></textarea>
            <p>Url for Image (optional): <input type="url" name="url" value="<?= $url ?>"></p>

            Education: <input type="button" id="addEducation" value="+">
            <?php
            $stmt = $pdo->prepare('SELECT * FROM Education JOIN Institution WHERE profile_id =:id AND Education.institution_id=Institution.institution_id ORDER BY rank');
            $stmt->execute(array(':id' => $_GET['profile_id']));

            $numEdu = 0;
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $numEdu++;
                $school = htmlentities($row['school']);
                $year = $row['year'];
//                $rank = $row['rank'];
            ?>
                <div id="edu<?= $numEdu ?>">
                <p>
                Year: <input type="text" name="edu_year<?= $numEdu ?>" value="<?= $year ?>">
                <input type="button" value="-" onclick="$('#edu<?= $numEdu ?>').remove(); return false;">
                </p>
                School: <input type="text" name="school<?= $numEdu ?>" class="school" value="<?= $school ?>">
                </div>
            <?php } ?>
            <p>
            <div id="education_fields"></div>
            </p>


            Position: <input type="button" id="addPosition" value="+">
            <?php
            $stmt = $pdo->prepare('SELECT * FROM Position WHERE profile_id =:id ORDER BY rank');
            $stmt->execute(array(':id' => $_GET['profile_id']));

            $numPos = 0;
            $pos_ids = array();
//            $ranks = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $numPos++;
                $description = htmlentities($row['description']);
                $posid = $row['position_id'];
                $year = $row['year'];
                $rank = $row['rank'];
                array_push($pos_ids, $posid);
 //               array_push($ranks, $rank);
            ?>
                <div id="position<?= $numPos ?>">
                <p>
                Year: <input type="text" name="year<?= $numPos ?>" value="<?= $year ?>">
                <input type="button" value="-" onclick="$('#position<?= $numPos ?>').remove(); return false;">
                </p>
                <textarea name="desc<?= $numPos ?>" rows="5" cols="80"><?= $description ?></textarea>
                </div>
            <?php } ?>
            <?php
                var_dump($pos_ids);
                $_SESSION['numPos'] = $numPos;
                $_SESSION['pos_ids'] = $pos_ids;
             ?>
            <div id="position_fields"></div>
            <p>
            <input type="submit" value="Save">
            <a href="index.php">Cancel</a>
            </p>
        </form>

        <script type="text/javascript">
            numPos = <?php echo $numPos; ?>;
            countPos = numPos;
            numEdu = <?php echo $numEdu; ?>;
            countEdu = numEdu;
            window && console.log('numPos '+numPos);
            window && console.log('numEdu '+numEdu);
 //           window && console.log(ranks);
            $(document).ready(function() {
                window && console.log('Document ready called');
                $('#addPosition').click(function(event) {
                    event.preventDefault();
                    if (countPos >= 9) {
                        alert('Maximum of nine positon entries exceeded');
                        return;
                    }
                    countPos++;

                    window && console.log('Adding Position '+countPos);

                    var source = $('#pos_template').html();
                    $('#position_fields').append(source.replace(/@COUNT@/g, countPos))
                });

                $('#addEducation').click(function(event) {
                    event.preventDefault();
                    if (countEdu >= 9) {
                        alert('Maximum of nine education entries exceeded');
                        return;
                    }
                    countEdu++;
                    window && console.log('Adding Education '+countEdu);

                    var source = $('#edu_template').html();
                    $('#education_fields').append(source.replace(/@COUNT@/g, countEdu))

                    $('.school').autocomplete({ source: "school.php" });
                });

            });
        </script>

        <script id="edu_template" type="text">
            <div id="edu@COUNT@">
                <p>
                Year: <input type="text" name="edu_year@COUNT@" value="">
                <input type="button" value="-" onclick="$('#edu@COUNT@').remove(); return false;">
                </p>
                School: <input type="text" name="school@COUNT@" class="school" value="">
            </div>

        </script>
        <script id="pos_template" type="text">
                <div id="position@COUNT@">
                <p>
                Year: <input type="text" name="year@COUNT@" value="">
                <input type="button" value="-" onclick="$('#position@COUNT@').remove(); return false;"></p>
                <textarea name="desc@COUNT@" rows="5" cols="80"></textarea>
        </script>


    </body>
</html>
