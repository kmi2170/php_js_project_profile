<?php
require_once "pdo.php";
session_start();

echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

if (!isset($_SESSION['user_id'])) {
    die('Access Denied');
}

if (! isset($_GET['profile_id'])) {
    $_SESSION['error'] = "Missing Profile Id";
    header("Location: index.php");
    return;
}

//$sql = "SELECT auto_id FROM autos WHERE auto_id=:id";
$sql = "SELECT profile_id, first_name, last_name, email FROM Profile WHERE profile_id=:id";
echo("<p>".$sql."</p>");
$stmt = $pdo->prepare($sql);
$stmt->execute(array(':id' => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ($row === false) {
    $_SESSION['error'] = "Bad Profile Id";
    header("Location: index.php");
    return;
}

if (isset($_POST['delete'])) {

    $sql = 'DELETE FROM Profile WHERE profile_id=:id';
    echo("<p>".$sql."</p>");
    $stmt = $pdo->prepare($sql);
    $row = $stmt->execute(array(':id' => $_GET['profile_id']));
    var_dump($row);

    $_SESSION['success'] = "Record Deleted";
    header("Location: index.php");
    return;
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            #msg {
                background-color: yellow;
            }
        </style>

    </head>
    <body>
        <?php
        $fname = htmlentities($row['first_name']);
        $lname = htmlentities($row['last_name']);
        $email = htmlentities($row['email']);
        ?>

        <h2>Confirmation</h2>
        <h3 >Are you sure you want to DELETE:
            <span id="msg">
            <?php echo $fname." / ".$lname." / ".$email." ?"; ?>
        </span></h3>
        <form method="post">
            <input type="submit" name="delete" value="Delete">
            <a href="index.php">Cancel</a>
        </form>
    </body>
</html>
